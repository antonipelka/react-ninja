import React from 'react';
import './App.css';
import { UserTable } from './UserTable';

export function App() {
  return (
    <div className="container mt-3">
      <UserTable />
    </div>
  );
}
