import { useEffect, useState } from 'react';

export const PromiseStatus = {
  Fetching: 'fetching',
  Resolved: 'resolved',
  Rejected: 'rejected',
};

export function usePromise(promiseCreator) {
  const [status, setStatus] = useState(PromiseStatus.Fetching);
  const [data, setData] = useState();
  useEffect(() => {
    (async () => {
      try {
        setData(await promiseCreator());
        setStatus(PromiseStatus.Resolved);
      } catch (error) {
        setData(error);
        setStatus(PromiseStatus.Rejected);
      }
    })();
  }, [promiseCreator]);

  return { status, data };
}
