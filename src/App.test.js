import React from 'react';
import { mount, shallow } from 'enzyme';
import { App } from './App';
import { DataTable } from './DataTable';
import { rowMapper } from './UserTable';
import { UserService } from './UserTable/UserService';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
i18n
  .use(initReactI18next)
  .init({
    lng: 'en',
    fallbackLng: 'en',

    // have a common namespace used around the full app
    ns: ['translations'],
    defaultNS: 'translations',

    interpolation: {
      escapeValue: false, // not needed for react!!
    },

    resources: { en: { translations: {} } },
  });

const rows = [
  {
    name1: 'Mads L. Klausen',
    email: 'MadsLKlausen@jourrapide.com',
    edit_path: 'http://google.com',
    per_id: 1
  },
  {
    name1: 'Alfred K. Krogh',
    email: 'AlfredKKrogh@armyspy.com',
    edit_path: 'http://google.com',
    per_id: 2
  },
  {
    name1: 'Silas L. Bertelsen',
    email: 'SilasLBertelsen@armyspy.com',
    edit_path: 'http://google.com',
    per_id: 3
  },
  {
    name1: 'Mia A. Johnsen',
    email: 'MiaAJohnsen@dayrep.com',
    edit_path: 'http://google.com',
    per_id: 4
  },
  {
    name1: 'Alfred S. Schou',
    email: 'AlfredSSchou@jourrapide.com',
    edit_path: 'http://google.com',
    per_id: 5
  }
];

it('renders without crashing', () => {
  shallow(<App />);
});

it('renders 5 rows', () => {
  const wrapper = mount(
    <DataTable
      data={rows}
      perPage={5}
      rowMapper={row => <tr key={row.per_id}></tr>}
      searchFilter={() => () => true}
    />
  );

  expect(wrapper.find('tr').length).toBe(5);
});

it('filters rows based on input', () => {
  const wrapper = mount(
    <DataTable
      data={rows}
      perPage={5}
      rowMapper={rowMapper}
      searchFilter={UserService.searchFilter}
    />
  );

  wrapper.find('input').simulate('change', { target: { value: 'k' } });

  expect(wrapper.find('tr').length).toBe(2);
});

it('handles incorrect regex searches', () => {
  const wrapper = mount(
    <DataTable
      data={rows}
      perPage={5}
      rowMapper={rowMapper}
      searchFilter={UserService.searchFilter}
    />
  );

  wrapper.find('input').simulate('change', { target: { value: '*' } });

  expect(wrapper.find('tr').length).toBe(0);
});
