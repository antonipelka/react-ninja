import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: {
      en: {
        translation: {
          'Loading...': 'Loading...',
          'Data fetching failed.': 'Data fetching failed.',
          'Search users': 'Search users',
          'No data found.': 'No data found.',
        }
      }
    },
    lng: 'en',
    fallbackLng: 'en',

    interpolation: {
      escapeValue: false
    }
  });

ReactDOM.render(<App />, document.getElementById('root'));
