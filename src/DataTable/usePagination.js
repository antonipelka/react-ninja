import { useMemo, useState } from 'react';

const calculateTotalNumberOfPages = (items, perPage) =>
  () => perPage ? Math.ceil(items.length / perPage) : 0;

export function usePagination({ items, perPage }) {
  const [current, changePage] = useState(0);
  const total = useMemo(
    calculateTotalNumberOfPages(items, perPage),
    [items, perPage],
  );

  return {
    changePage,
    current,
    perPage,
    total,
  };
}