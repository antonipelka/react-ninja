import React from 'react'
import { PageLink } from './PageLink'

export const Pagination = ({ controller: { current, total, changePage } }) => {
  const pages =
    Array
      .from(Array(total).keys())
      .map(pageNumber => (
        <PageLink
          key={pageNumber}
          currentPageNumber={current}
          pageNumber={pageNumber}
          onChange={changePage}
        />
      ))

  if (pages.length <= 1) {
    return null;
  }

  return (
    <ul className="pagination">
      {pages}
    </ul>
  );
}
