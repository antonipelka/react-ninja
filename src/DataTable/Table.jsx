import React from 'react';
import { useTranslation } from 'react-i18next';

export function Table({ items, rowMapper }) {
  const { t } = useTranslation();

  if (items.length === 0) {
    return t('No data found.');
  }

  return (
    <table>
      <tbody>
        {items.map(rowMapper)}
      </tbody>
    </table>
  );
}
