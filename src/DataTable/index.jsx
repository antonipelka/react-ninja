import React from 'react';
import { Pagination } from './Pagination';
import { Search } from './Search';
import { Table } from './Table';
import { useDataTable } from './useDataTable';

export function DataTable({
  data,
  perPage,
  rowMapper,
  searchFilter,
}) {
  const {
    items,
    pagination,
    changeSearchQuery,
  } = useDataTable({ data, perPage, searchFilter });

  return (
    <div>
      <Search onSearch={changeSearchQuery} />
      <Table items={items} rowMapper={rowMapper} />
      <Pagination controller={pagination} />
    </div>
  );
}
