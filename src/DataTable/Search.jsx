import React from 'react'
import { useTranslation } from 'react-i18next';

export const Search = (props) => {
  const { t } = useTranslation();
  const { onSearch } = props

  return (
    <div className="p-b-1">
      <input
        type="search"
        className="form-control"
        placeholder={t('Search users')}
        onChange={onSearch} />
    </div>
  )
};
