import { useState, useMemo, useCallback } from 'react';
import { usePagination } from './usePagination';

const getItemsRange = (currentPageNumber, perPage) => {
  const startIndex = currentPageNumber * perPage;
  return [startIndex, startIndex + perPage];
}

export function useDataTable({ data: items = [], perPage, searchFilter }) {
  const [query, setQuery] = useState('');

  const filteredItems = useMemo(
    () => items.filter(searchFilter(query)),
    [items, query, searchFilter],
  );

  const pagination = usePagination({
    items: filteredItems,
    perPage,
  });

  const slicedItems = useMemo(
    () => filteredItems.slice(...getItemsRange(pagination.current, perPage)),
    [filteredItems, pagination, perPage]
  );

  const changeSearchQuery = useCallback((event) => {
    const newQuery = event.target.value;
    setQuery(newQuery);
    pagination.changePage(0);
  }, [pagination]);

  return {
    items: slicedItems,
    changeSearchQuery,
    pagination,
  };
}