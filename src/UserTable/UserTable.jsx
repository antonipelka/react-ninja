import React from 'react';
import { PromiseStatus, usePromise } from '../hooks/usePromise';
import { UserService } from './UserService';
import { DataTable } from '../DataTable';
import { UserRow } from './UserRow';
import { useTranslation } from 'react-i18next';

export function rowMapper(row) {
  return <UserRow key={row.per_id} row={row} />;
}

export function UserTable() {
  const { t } = useTranslation();
  const { status, data } = usePromise(UserService.get);

  if (status === PromiseStatus.Fetching) {
    return (
      <div className="container mt-3">
        {t('Loading...')}
      </div>
    );
  }

  if (status === PromiseStatus.Rejected) {
    return (
      <div className="container mt-3">
        {t('Data fetching failed.')}
      </div>
    );
  }

  return (
    <DataTable
      data={data}
      perPage={5}
      rowMapper={rowMapper}
      searchFilter={UserService.searchFilter}
    />
  );
}