export const UserService = {
  async get() {
    return await JSON.parse(document.getElementById('user-data').dataset.users);
  },
  searchFilter: (query) =>
    ({ name1, email }) => {
      try {
        const regex = new RegExp(query.toLowerCase());
        return (
          name1.toLowerCase().search(regex) > -1 ||
          email?.toLowerCase().search(regex) > -1
        );
      } catch {
        return false;
      }
    },
};
